export function copy(obj) {
  return JSON.parse(JSON.stringify(obj));
}

export function isObject(val) {
  if (val === null || val instanceof EventTarget || Array.isArray(val)) {
    return false;
  }
  return typeof val === "function" || typeof val === "object";
}

export function logit(...args) {
  console.debug(...args);
  return args[0];
}

export function range(first, last) {
  const arr = [];
  for (let index = first; index <= last; index++) {
    arr.push(index);
  }
  return arr;
}

export function queryObject(obj, dataPath) {
  let curValue = obj;
  dataPath.forEach((path) => {
    curValue = curValue[path];
  });
  return curValue;
}
