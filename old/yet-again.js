import { isObject } from "./helpers.js";

// This is a standin for the real state management later on.
Array.prototype.list = function (fn) {
  return $(this.map(fn));
};

export function $(structure) {
  const { type, attrs, content } = getStructureParts(structure);

  const node = type
    ? document.createElement(type)
    : document.createDocumentFragment();

  attrs.on &&
    Object.entries(attrs.on).map(([eventName, handler]) => {
      node.addEventListener(eventName, handler);
    });
  delete attrs.on;

  attrs.style &&
    Object.entries(attrs.style).map(([styleName, value]) => {
      node.style[styleName] = value;
    });
  delete attrs.style;

  Object.entries(attrs).map(([attr, value]) => {
    if (typeof value === "function") {
      value((newValue) => node.setAttribute(attr, newValue));
    } else {
      node.setAttribute(attr, value);
    }
  });

  const convertedItems = content.map((item) => {
    if (Array.isArray(item)) {
      return $(item);
    } else if (typeof item == "function") {
      const node = document.createTextNode();
      return item((newValue) => (node.textContent = newValue.toString()));
    } else {
      return document.createTextNode(item.toString());
    }
  });
  convertedItems.forEach((item) => {
    node.appendChild(item);
  });

  return node;
}

// "structure" data type described elsewhere ['div', {on: {click() {}}},
// ['span', 'hi']]. Because type and attrs are optional, we must do work to
// figure out the real type (explicit, implit, or none), the attrs (provided or
// empty), and the content (includes or not includes the first and second items)
function getStructureParts(structure) {
  const first = structure[0];
  const typeExplicit = typeof first === "string";

  const attrsIndex = typeExplicit ? 1 : 0;
  const possibleAttrs = structure[attrsIndex];
  const attrsProvided = isObject(possibleAttrs);

  const typeImplicit = !typeExplicit && attrsProvided;

  const indexContentStarts =
    0 + (typeExplicit ? 1 : 0) + (attrsProvided ? 1 : 0);

  return {
    type: typeExplicit ? first : typeImplicit ? "span" : null,
    attrs: attrsProvided ? possibleAttrs : {},
    content: structure.slice(indexContentStarts),
  };
}
