import { createDB, query, queryObjects, queryWhere } from "./datalog.js";

const inquiry = {
  find: ["?name"],
  where: [
    // ["?gizmo", "gizmo/owner", "?owner"], //
    ["?", "person/name", "?name"],
  ],
};

function toTriplet(possibleTriplet) {
  if (possibleTriplet.length < 1) {
    throw new Error(
      "Triplet should have at least 1 item in it: " +
        JSON.stringify(possibleTriplet)
    );
  }
  const missingParts = 3 - possibleTriplet.length;
  const defaultValues = ["?", "state/keyword"];
  const valuesToInsert = defaultValues.slice(0, missingParts);
  return valuesToInsert.concat(possibleTriplet);
}

const db = createDB([
  [1, "type", "gizmo"],
  [1, "gizmo/name", "clinker"],
  [1, "gizmo/creator", 11],
  [1, "gizmo/owner", 10],
  [1, "gizmo/inventory", 101],

  [2, "type", "gizmo"],
  [2, "gizmo/name", "kaputz"],
  [2, "gizmo/creator", 11],
  [2, "gizmo/owner", 12],
  [2, "gizmo/inventory", 101],

  [10, "type", "person"],
  [10, "person/name", "George"],
  [11, "type", "person"],
  [11, "person/name", "Alfred"],
  [12, "type", "person"],
  [12, "person/name", "Susan"],
]);

//////////
//////////
//////////
//////////
//////////
//////////

const inventoryId = 101;

const inquiry2 = {
  find: ["?name", "?creator", "?owner"],
  where: [
    ["?gizmoId", "type", "gizmo"], //
    ["?gizmoId", "gizmo/inventory", inventoryId],
    ["?gizmoId", "gizmo/name", "?name"],
    ["?gizmoId", "gizmo/owner", "?ownerId"],
    ["?ownerId", "person/name", "?owner"],
    ["?gizmoId", "gizmo/creator", "?creatorId"],
    ["?creatorId", "person/name", "?creator"],
  ],
  sort: "id",
};

//////////
//////////
//////////

const theId = 2;
const inquiry3 = [
  [theId, "gizmo/inventory", inventoryId], //
];

// console.log(JSON.stringify(db, null, 2));
console.log(queryObjects(inquiry3, db));

//////////
//////////
//////////
//////////
//////////
//////////

function view(inquiry, sort, callback) {
  //
}

function parseHTML(html) {
  var t = document.createElement("template");
  t.innerHTML = html;
  return t.content;
}
