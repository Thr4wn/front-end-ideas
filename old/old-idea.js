import { strict as assert } from "node:assert";
import { query, queryObjects } from "./datalog";

//

function __() {}

function trigger() {}

const inventoryId = 101;

function use() {}

const db = null;
const state = null;

$$(($node) => {
  use(["gizmo/name", "gizmo/owner", "gizmo/creator"]);

  const it = {
    onChange: ["gizmo/name", "gizmo/owner", "gizmo/creator"],
    find: [id, "?name", "?owner", "?creator"],
    where: [
      [id, "gizmo/inventory", inventoryId],
      [id, "gizmo/name", "?name"],
      [id, "gizmo/owner", "?owner"],
      [id, "gizmo/creator", "?creator"],
    ],
  };

  onChange(["gizmo/name", "gizmo/owner", "gizmo/creator"], {
    find: ["?id", "?name", "?owner", "?creator"],
    where: [
      ["?id", "gizmo/inventory", inventoryId],
      ["?id", "gizmo/name", "?name"],
      ["?id", "gizmo/owner", "?owner"],
      ["?id", "gizmo/creator", "?creator"],
    ],
  });

  state.onChange("gizmo/", (db) => {
    const gizmos = queryObjects(
      {
        find: ["?gizmoId", "?name", "?owner", "?creator"],
        where: [
          ["?gizmoId", "gizmo/inventory", inventoryId],
          ["?gizmoId", "gizmo/name", "?name"],
          ["?gizmoId", "gizmo/owner", "?owner"],
          ["?gizmoId", "gizmo/creator", "?creator"],
        ],
      },
      db
    );
  });

  state.onChange("gizmo/", (triplets) => {
    const gizmos = queryObjects({
      find: ["?gizmoId", "?name", "?owner", "?creator"],
      where: [
        ["?gizmoId", , , tranId],
        ["?gizmoId", "gizmo/inventory", inventoryId],
        ["?gizmoId", "gizmo/name", "?name"],
        ["?gizmoId", "gizmo/owner", "?owner"],
        ["?gizmoId", "gizmo/creator", "?creator"],
      ],
    });
  });
});

$$(($node) => {
  trigger("gizmo/", (triplets, prevValues) => {
    // const gizmoIdsWithinInventory = triplets
    //   .map(([id]) => id)
    //   .filter((id) => {
    //     return queryObjects([[id, "gizmo/inventory", inventoryId]]).length > 0;
    //   });

    const gizmos = triplets.map(([id]) => {
      return {
        id,
        ...queryObjects([
          [id, "gizmo/inventory", inventoryId],
          [id, "gizmo/name", "?name"],
          [id, "gizmo/owner", "?owner"],
          [id, "gizmo/creator", "?creator"],
        ])[0],
      };
    });

    // $node.appendChildren(
    $node.ensureChildren(
      gizmos.map((gizmo) =>
        $([
          "span",
          "gizmo name is ",
          gizmo.name,
          " and owner is",
          gizmo.owner,
          "and creator is",
          gizmo.creator,
        ])
      )
    );
  });
});

// do I want the update events? I guess so. I don't want to trigger dozens of
// events for a single change.

const inventories = $$([["inventory"]], (inventory) => {
  const $gizmos = $$("gizmo", ($node, gizmo) =>
    html(["span", "gizmo id is ", gizmo.id])
  );

  /* wait a second. I want to atomically react to "state object". However, these
     triplets are parts of entities. So, should I auto collect all attributes
     from entity ids?

     I don't really want to query things in general.

     But I do want to say "the gizmos for this inventory"

     ooooh. But why not say that only when the specific attribute of "name"
     changes? !!


     hmm. The point is to identify "state things", and ensure that when they
     change, there are callbacks happening.

     But creating contexts means that I don't actually identify 'things'.

     I guess I could 'mark' every triplet discovered this way. But then how
     would I also handle additions? (removals can check marks as well). But with
     additions, surely I cannot re-execute every inquiry!


     Well, maybe we're back to manual tagging with specific keywords.
     "state/tag". If I can can add multiple tags to every entity, then that
     means that ... what?

     I guess it means that every update will find that entity id, then run a
     query to find tags, and then it knows what hooks to run. Every delete and
     every addition can check tags to trigger hooks as well.

     The hooks can either:
     * return just the entity id
     * practively get all attributes.
     * run an input query??
    


     ok. So now I can tag things.

     However, I see from above that I only want gizmos that are within inventory X.
  */

  const $gizmos2 = view(
    {
      find: ["?name", "?creator", "?owner"],
      where: [
        ["?id", "type", "gizmo"], //
        ["?id", "gizmo/inventory", inventoryId],
        ["?id", "gizmo/name", "?name"],
        ["?id", "gizmo/owner", "?owner"],
        ["?id", "gizmo/creator", "?creator"],
      ],
      sort: "id",
    },
    ($node, results) => {
      results.map(([name, creatorId, ownerId]) => {
        return html([
          "span",
          "gizmo name is ",
          gizmo.name,
          " and owner is",
          gizmo.owner,
          "and creator is",
          gizmo.creator,
        ]);
      });
    }
  );

  return $([
    "div",
    ["h2", inventory.name],
    "Here's the list of gizmos",
    gizmos,
  ]);
});

function doTest() {
  assert.equal(html(["div"]), "<div></div>");
  assert.equal(html(["div", { id: "foo" }]), `<div id="foo"></div>`);
  assert.equal(
    html(["div", { id: "foo" }, "test"]),
    `<div id="foo">test</div>`
  );
  assert.equal(
    html(["div", { id: "foo" }, ["span", "hi"]]),
    `<div id="foo"><span>hi</span></div>`
  );
}
doTest();

function html([type, _attrs, ..._content]) {
  // treat _attrs as optional
  const [attrs, content] = isObject(_attrs)
    ? [_attrs, _content]
    : [{}, [_attrs, ..._content]];

  const attributesHtml = Object.entries(attrs)
    .map(([name, value]) => name + "=" + enquote(value))
    .join(" ");

  const spacer = attributesHtml ? " " : "";

  const contentHtml = content.map((item) => {
    return Array.isArray(item) ? html(item) : item;
  });

  return `<${type}${spacer}${attributesHtml}>${contentHtml}</${type}>`;
}

function enquote(value) {
  return `"${value}"`;
}

function isObject(val) {
  if (val === null) {
    return false;
  }
  return typeof val === "function" || typeof val === "object";
}

//////////////

// function _2([element, ...content]) {
//   const [type, attrs] =
//     typeof element === "string" ? [element, {}] : [element.type, element];

//   if (typeof attrs === "string") {
//     const elementType = attrs;
//     const attrs = {};
//   } else {
//   }
// }
