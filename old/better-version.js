/////////////////////////////////
// App
/////////////////////////////////

function App(initState = {}) {
  let state = copy(initState);

  function TestView(a, b) {
    if (!b) {
      transformation = () => {};
      render = (i) => a;
    } else {
      transformation = a;
      render = b;
    }
    const elementStructure = render(transformation(state));
    const element = makeNode(elementStructure);
    return {
      element,
      view: {
        use(fn) {
          return fn(transformation(state));
        },
      },
    };
  }
  function View(...args) {
    return TestView(...args).element;
  }

  async function update(fn) {
    const newState = copy(state);
    await fn(newState);
    state = newState;
    return;
  }

  return {
    View,
    TestView,
    update,
  };
}

function makeNode(structure) {
  const template = document.createElement("template");
  const postProcessors = [];
  template.innerHTML = toHtml(structure, postProcessors);
  const newNode = template.content.cloneNode(true);
  postProcessors.forEach((fn) => fn(newNode));
  return newNode;
}

/////////////////////////////////
// toHtml
/////////////////////////////////

function toHtml(structure, postProcessors = []) {
  const { type, attrs, content } = getStructureParts(structure);

  const attributesHtml = attrsToHtml(attrs, postProcessors);
  const contentHtml = contentToHtml(content, postProcessors);

  const wrapWithTag = (type, attributesHtml, html) =>
    `<${type} ${attributesHtml}>${html}</${type}>`;

  return type ? wrapWithTag(type, attributesHtml, contentHtml) : contentHtml;
}

// "structure" data type described elsewhere. Because type and attrs are
// optional, we must do work to figure out the real type (explicit, implit, or
// none), the attrs (provided or empty), and the content (includes or not
// includes the first and second items)
function getStructureParts(structure) {
  let [first, second, ...rest] = structure;

  const typeExplicit = typeof first === "string";

  const attrsIndex = typeExplicit ? 1 : 0;
  const possibleAttrs = structure[attrsIndex];
  const attrsProvided = isObject(possibleAttrs);

  const typeImplicit = !typeExplicit && attrsProvided;

  const indexContentStarts =
    0 + (typeExplicit ? 1 : 0) + (attrsProvided ? 1 : 0);

  return logit({
    type: typeExplicit ? first : typeImplicit ? "span" : null,
    attrs: attrsProvided ? possibleAttrs : {},
    content: structure.slice(indexContentStarts),
  });
}

function attrsToHtml(attrs, postProcessors) {
  const on = attrs.on || {};
  delete attrs.on;

  const style = attrs.style;
  delete attrs.style;

  const attributeStrings = [
    ...Object.entries(attrs).map(([name, value]) => `${name}="${value}"`),
    style
      ? `style='${Object.entries(style)
          .map(([name, value]) => {
            return `${name}:${value}`;
          })
          .join(";")}'`
      : "",
    ...Object.entries(on).map(([eventName, handler]) => {
      const { name, html } = postProcessingMarker("attribute");
      postProcessors.push((node) => {
        node
          .querySelector(`[data-handler-id=${name}]`)
          .addEventListener(eventName, handler);
      });
      return html;
    }),
  ];
  const attributesHtml = attributeStrings.join("");
  return attributesHtml;
}

function contentToHtml(content, postProcessors) {
  return content
    .map((item) => {
      const itemType =
        (Array.isArray(item) && "content") ||
        (item instanceof Node && "node") ||
        "default";

      const logic = {
        datapoint() {},
        content() {
          return toHtml(item, postProcessors);
        },
        node() {
          const { name, html } = postProcessingMarker("slot");
          postProcessors.push((node) => {
            node.querySelector(`slot[name=${name}]`).replaceWith(item);
          });
          return html;
        },
        default() {
          return item && item.toString();
        },
      };
      return logic[itemType]();
    })
    .join("");
}

function postProcessingMarker(type) {
  const name = "marker_" + Math.random().toString().replace(".", "");
  const html =
    type === "slot"
      ? `<slot name="${name}"></slot>`
      : ` data-handler-id="${name}" `;
  console.debug("adding marker", name);
  return { name, html };
}

/////////////////////////////////
// helpers
/////////////////////////////////

function copy(obj) {
  return JSON.parse(JSON.stringify(obj));
}

function isObject(val) {
  if (val === null || val instanceof EventTarget || Array.isArray(val)) {
    return false;
  }
  return typeof val === "function" || typeof val === "object";
}

function logit(...args) {
  console.debug(...args);
  return args[0];
}

function range(first, last) {
  const arr = [];
  for (let index = first; index <= last; index++) {
    arr.push(index);
  }
  return arr;
}
