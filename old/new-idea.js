const state = {
  message: "I'm cool",
  profiles: [{ name: "George", favoriteNumber: 0 }],
};

const dataPointSymbol = Symbol("data point");

const updateHandlers = [];

/////////

const main = $([
  "div",
  "hello world",
  [
    "p",
    "here is a list of profiles",
    $(
      // (_) => ({ profiles: _.profiles, message: _.message }),
      [
        "div",
        _("message"),
        // profiles.map((p) => [
        //   ["div", "name: ", p.name],
        //   ["div", "favoriate number: ", p.favoriteNumber],
        // ]),
      ]
    ),
    $(["input", { type: "text" }]),
  ],
]);

//////

function _(...args) {
  args[dataPointSymbol] = true;
  return args;
}

function update(fn) {
  fn(state);
  updateHandlers.forEach((h) => h(state));
}

function $(...args) {
  if (typeof args[0] === "function") {
    return dynamicRender(...args);
  } else {
    return render(...args);
  }
}

function dynamicRender(getter, viewer) {
  const data = getter(state);
  return viewer(data);
}

function htmlFactory(slots = {}) {
  function toHtml(input) {
    const [type, _attrs, ..._content] = input;

    // If first param is array, then assume that it's an array of the 'real'
    // dom array structures
    if (Array.isArray(type)) {
      return input.map((i) => toHtml(i)).join("");
    }

    // treat _attrs as optional
    const [attrs, content] = isObject(_attrs)
      ? [_attrs, _content]
      : [{}, [_attrs, ..._content]];

    const on = attrs.on || {};
    delete attrs.on;

    const attributeStrings = [
      ...Object.entries(attrs).map(([name, value]) => `${name}="${value}"`),
      ...Object.entries(on).map(([eventName, handler]) =>
        newSlot({ handler, eventName }, false)
      ),
    ];

    const attributesHtml = attributeStrings.join("");

    const spacer = attributesHtml ? " " : "";

    const contentHtml = content
      .map((item) => {
        return item && item[dataPointSymbol]
          ? newSlot({ dataPath: item })
          : Array.isArray(item)
          ? toHtml(item)
          : item instanceof Node
          ? newSlot({ fragment: item })
          : item;
      })
      .join("");

    return `<${type}${spacer}${attributesHtml}>${contentHtml}</${type}>`;
  }
  function newSlot(slotInfo, slot = true) {
    const slotName = "slot_" + Math.random().toString().replace(".", "");
    console.log("adding slot", slotName, slotInfo);
    slots[slotName] = slotInfo;
    return slot
      ? `<slot name="${slotName}"></slot>`
      : ` data-handler-id="${slotName}" `;
  }

  return { toHtml };
}

function render(vdom) {
  console.log("rendering", vdom);
  const slots = {};
  const { toHtml } = htmlFactory(slots);

  const template = document.createElement("template");
  const html = toHtml(vdom);
  template.innerHTML = html;
  const newNode = template.content.cloneNode(true);

  console.log("slots are", slots);
  Object.entries(slots).forEach(
    ([slotName, { emitter, fragment, eventName, handler, dataPath }]) => {
      if (emitter) {
        emitter.addEventListener("change", (data) => {
          newNode.querySelector(`slot[name=${slotName}]`).textContent =
            data.detail;
        });
      }
      if (dataPath) {
        console.log("doing datapath slot work", slotName, dataPath);
        updateHandlers.push((state) => {
          let curValue = state;
          dataPath.forEach((path) => {
            curValue = curValue[path];
          });
          console.log("assigning slot", slotName, curValue);
          const elm = document.querySelector(`slot[name=${slotName}]`);
          if (elm) {
            console.log("elm", elm);
            elm.textContent = curValue;
          } else {
            console.log("not found");
          }
        });
      }
      if (fragment) {
        newNode.querySelector(`slot[name=${slotName}]`).appendChild(fragment);
      }
      if (eventName && handler) {
        newNode
          .querySelector(`[data-handler-id=${slotName}]`)
          .addEventListener(eventName, handler);
      }
    }
  );

  return newNode;
}

function isObject(val) {
  if (val === null || val instanceof EventTarget || Array.isArray(val)) {
    return false;
  }
  return typeof val === "function" || typeof val === "object";
}

function logit(...args) {
  console.log(...args);
  return args[0];
}
