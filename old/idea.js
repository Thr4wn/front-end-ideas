/*
 * const instance = new StateEvent();
 * instance.addEventListener("change", (event) => {
 *   console.log('Instance fired "change" event:', event.detail);
 * });
 * instance.changed(42);
 */
class StateEvent extends EventTarget {
  constructor(name) {
    super();
    this.key = new Symbol(name);
  }
  changed(data) {
    this.dispatchEvent(new CustomEvent("change", { detail: data }));
  }
}

function htmlFactory(slots = {}) {
  function toHtml([type, _attrs, ..._content]) {
    // treat _attrs as optional
    const [attrs, content] = isObject(_attrs)
      ? [_attrs, _content]
      : [{}, [_attrs, ..._content]];

    const on = attrs.on || {};
    delete attrs.on;

    const attributeStrings = [
      ...Object.entries(attrs).map(([name, value]) => `${name}="${value}"`),
      ...Object.entries(on).map(([eventName, handler]) =>
        newSlot({ handler, eventName }, false)
      ),
    ];

    const attributesHtml = attributeStrings.join("");

    const spacer = attributesHtml ? " " : "";

    const contentHtml = content
      .map((item) => {
        return Array.isArray(item)
          ? toHtml(item)
          : item instanceof StateEvent
          ? newSlot({ emitter: item })
          : item instanceof Node
          ? newSlot({ fragment: item })
          : item;
      })
      .join("");

    return `<${type}${spacer}${attributesHtml}>${contentHtml}</${type}>`;
  }
  function newSlot(slotInfo, slot = true) {
    const id = "slot_" + Math.random().toString().replace(".", "");
    slots[id] = slotInfo;
    return slot
      ? `<slot name="${id}">SLOT</slot>`
      : ` data-handler-id="${id}" `;
  }

  return { toHtml };
}

function $(vdom) {
  const slots = {};
  const { toHtml } = htmlFactory(slots);

  const template = document.createElement("template");
  const html = toHtml(vdom);
  template.innerHTML = html;
  const newNode = template.content.cloneNode(true);

  // console.log("the html", html);

  Object.entries(slots).forEach(
    ([slotName, { emitter, fragment, eventName, handler }]) => {
      if (emitter) {
        emitter.addEventListener("change", (data) => {
          newNode.querySelector(`slot[name=${slotName}]`).textContent =
            data.detail;
        });
      }
      if (fragment) {
        newNode.querySelector(`slot[name=${slotName}]`).appendChild(fragment);
      }
      if (eventName && handler) {
        console.log("trying to add listener", eventName, handler);
        newNode
          .querySelector(`[data-handler-id=${slotName}]`)
          .addEventListener(eventName, handler);
      }
    }
  );

  return newNode;
}

function StateFactory() {
  const db = new Map();

  class StateWrapper extends Map {
    constructor(...args) {
      super(...args);
      this.updates = [];
    }
    update(key, value) {
      this.set(key, value);
      this.updates.push(key);
    }
  }

  class Publisher extends EventTarget {
    publishUpdate(fn) {
      this.dispatchEvent(new CustomEvent("update", { detail: { fn } }));
    }
  }

  Publisher.addEventListener("update", ({ fn }) => {
    const wrappedDb = new StateWrapper(db);
    fn(wrappedDb);
    // emit all state events
    wrappedDb.updates;
  });

  function update(fn) {
    Publisher.publishUpdate(fn);
  }

  function read(keys) {
    // if _any_ of the keys are updated, then call someone.
  }

  function create(initialValue) {
    const newKey = new StateEvent();
    Publisher.publishUpdate((db) => {
      db.set(newKey, initialValue);
    });
    return newKey;
  }
}

// function _(initialValue) {
// }

function isObject(val) {
  if (val === null || val instanceof EventTarget || Array.isArray(val)) {
    return false;
  }
  return typeof val === "function" || typeof val === "object";
}
