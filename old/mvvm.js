import { copy, isObject, logit, queryObject, range } from "./helpers.js";

export function Controller(initState = {}) {
  let state = copy(initState);
  const refreshers = [];

  return {
    View,
    $: View,
    PrivateView,
    update,
    makeNode,
  };

  function PrivateView(a, b) {
    let transformation, render;
    if (!b) {
      transformation = () => {};
      render = (i) => a;
    } else {
      transformation = a;
      render = b;
    }
    const elementStructure = render(transformation(state));
    const element = makeNode(elementStructure, refreshers);
    return {
      element,
      view: {
        use(fn) {
          return fn(transformation(state));
        },
      },
    };
  }
  function View(...args) {
    return PrivateView(...args).element;
  }

  async function update(fn) {
    const newState = copy(state);
    await fn(newState);
    state = newState;
    refresh();
    return;
  }

  function refresh() {
    refreshers.forEach((fn) => {
      fn();
    });
  }

  function makeNode(structure, refreshers = []) {
    const template = document.createElement("template");
    const postProcessors = [];

    template.innerHTML = toHtml(
      logit(structure, "structure within makeNode"),
      postProcessors,
      refreshers
    );
    const newNode = template.content.cloneNode(true);
    postProcessors.forEach((fn) => fn(newNode));
    return newNode;
  }

  /////////////////////////////////
  // toHtml
  /////////////////////////////////

  function toHtml(structure, postProcessors = [], refreshers = []) {
    const { type, attrs, content } = getStructureParts(structure);

    const attributesHtml = attrsToHtml(attrs, postProcessors, refreshers);
    const contentHtml = contentToHtml(content, postProcessors, refreshers);

    const wrapWithTag = (type, attributesHtml, html) =>
      `<${type} ${attributesHtml}>${html}</${type}>`;

    return type ? wrapWithTag(type, attributesHtml, contentHtml) : contentHtml;
  }

  // "structure" data type described elsewhere. Because type and attrs are
  // optional, we must do work to figure out the real type (explicit, implit, or
  // none), the attrs (provided or empty), and the content (includes or not
  // includes the first and second items)
  function getStructureParts(structure) {
    let [first, second, ...rest] = structure;

    const typeExplicit = typeof first === "string";

    const attrsIndex = typeExplicit ? 1 : 0;
    const possibleAttrs = structure[attrsIndex];
    const attrsProvided = isObject(possibleAttrs);

    const typeImplicit = !typeExplicit && attrsProvided;

    const indexContentStarts =
      0 + (typeExplicit ? 1 : 0) + (attrsProvided ? 1 : 0);

    return logit({
      type: typeExplicit ? first : typeImplicit ? "span" : null,
      attrs: attrsProvided ? possibleAttrs : {},
      content: structure.slice(indexContentStarts),
    });
  }

  function attrsToHtml(attrs, postProcessors) {
    const on = attrs.on || {};
    delete attrs.on;

    const style = attrs.style;
    delete attrs.style;

    const attributeStrings = [
      ...Object.entries(attrs).map(
        ([name, value]) => `${name}="${getValue(value)}"`
      ),
      style
        ? `style='${Object.entries(style)
            .map(([name, value]) => {
              return `${name}:${getValue(value)}`;
            })
            .join(";")}'`
        : "",
      ...Object.entries(on).map(([eventName, handler]) => {
        const { name, html } = postProcessingMarker("attribute");
        postProcessors.push((node) => {
          node
            .querySelector(`[data-handler-id=${name}]`)
            .addEventListener(eventName, handler);
        });
        return html;
      }),
    ];
    const attributesHtml = attributeStrings.join("");
    return attributesHtml;
  }

  function getValue(value) {
    return (value.isDatapoint && queryObject(state, value)) || value;
  }

  function contentToHtml(content, postProcessors, refreshers) {
    return content
      .map((item) => {
        const itemType =
          (Array.isArray(item) && item.isDatapoint && "datapoint") ||
          (Array.isArray(item) && "content") ||
          (item instanceof Node && "node") ||
          "default";

        const logic = {
          datapoint() {
            const { name, html } = postProcessingMarker("slot");

            postProcessors.push((node) => {
              const subNode = node.querySelector(`slot[name=${name}]`);

              const thisRefresh = () => {
                const value = queryObject(state, item);
                subNode.innerHTML = "YESS";
              };
              refreshers.push(thisRefresh);
              thisRefresh;
            });

            return html;
          },
          content() {
            return toHtml(
              logit(item, "item being recursed"),
              postProcessors,
              refreshers
            );
          },
          node() {
            const { name, html } = postProcessingMarker("slot");
            postProcessors.push((node) => {
              node.querySelector(`slot[name=${name}]`).replaceWith(item);
            });
            return html;
          },
          default() {
            return item && getValue(item);
          },
        };
        return logic[itemType]();
      })
      .join("");
  }
}

export function _(...args) {
  args.isDatapoint = true;
  return args;
}

function postProcessingMarker(type) {
  const name = "marker_" + Math.random().toString().replace(".", "");
  const html =
    type === "slot"
      ? `<slot name="${name}"></slot>`
      : ` data-handler-id="${name}" `;
  console.debug("adding marker", name);
  return { name, html };
}
