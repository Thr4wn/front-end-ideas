/////////////////////////////////////
/// Examples using testable-components.js
/////////////////////////////////////

import {
  ui,
  doSync,
  onSync,
  sync,
  reader,
  Writer,
  render,
  Model,
} from "./lib.js";

/////////////////////////////////////
/// View Examples (without, and with, data binding)
/////////////////////////////////////

// Views are simple to make. Input "props" are whatever you need. Then return
// the structure. (At the end, you'll see the 'render' function that parses this
// output.)
const infoBlock = ({ content }) => [
  "div",
  { style: { margin: "10px" }, "background-color": "gray" },
  "INFO",
  ["hr"],
  ["div", content],
];
// JUST return an array. Don't put logic into the view. When a component needs
// more javascript logic, then scroll down below to see how to use a view model
// in this system.

// DATA BINDING
// ------------

// For handling events, a design pattern used by this library is the "sync"
// pattern.
//
// 1. DOM event handlers are oneliners: use the event API to grab the value of
//    real interest, and then fire a custom 'sync' event with that value.
//
// 2. Next, attach business logic to the 'sync' event. Now the business logic
//    (viewmodel function) doesn't have to couple with event APIs!
const textInput = ({ content }) => [
  // binding value to 'content'...
  { value: content },
  //       ^^^^^^^
  // If 'content' is a "model function", then this will actually 'bind' the
  // value attribute to the ever-changing value of 'content'. In that case,
  // 'content' will be a _function_ bound to a model. This function will read or
  // update content (dependending on if an argument is passed in or not).
  //
  // When the render function sees a _function_ passed as the value for an
  // attribute it will do the magic of creating event listeners to the model's
  // updates, and then update the attribute 'value' as needed.

  { on: { change: doSync((event) => event.target.value) } },
  //              ^^^^^^^^^  that is shorthand for this below...
  //
  // {
  //   on: {
  //     change(event) {
  //       event.target.dispatchEvent(
  //         "sync",
  //         new CustomEvent("sync", { detail: event.target.value })
  //       );
  //     },
  //   },
  // },
  //
  // For the record ..... I didn't invent browser custom event APIs.

  onSync(content),
  // ^^ that is shorthand for this below...
  //
  // {
  //   on: {
  //     sync(event) {
  //       content(event.detail);
  //     },
  //   },
  // },

  // Remember, if 'content' is a model function, then calling it with a value
  // will will trigger an update to that value in the model.
];

// Example Widget Library
// ----------------------

// Many will want to create their own base widgets. Here's how that could work:

const checkboxWidget = ({ value, label, name = Math.random() }) => [
  // Notice: when the first element is not a tag name, a document fragment will
  // end up being created/used instead.
  ["label", { for: name }, label],
  [
    "input",
    { type: "checkbox" },
    { name },

    sync("change", content),
    // That's shorthand for...
    //
    // { on: { change: doSync(inferValue) } },
    // onSync(content),

    // Where inferValue will first check for event.target.value, and then check
    // other common places for inferring the real value.
  ],
];

const myView = ({ darkMode }) => [
  checkboxWidget({ value: darkMode, label: "Enable dark mode?" }),
];

// Or you can use the UI library that comes with this package:

const checkboxWidget_2 = (props) => [ui.label(props), ui.checkbox(props)];

const myView_2 = ({ darkMode }) => [
  checkboxWidget_2({
    value: darkMode,
    label: "Enable dark mode?",
    attrs: { class: "special" },
  }),
  // Did you notice the "attrs" value above? The builtin UI library will
  // automatically take the "attrs" prop, and add those as attributes.
];

////////////////////////////////////
/// Using a View Model with a View
////////////////////////////////////

const messageDashboard =
  () =>
  ({ header, models: { messageModel } }, testMode) => {
    const messageFormatted = new Reader(
      messageModel,
      ({ isShouting, message }) =>
        isShouting ? message.toUpperCase() : message
    );
    const showProfile = new Reader(
      messageModel,
      ({ profile }) =>
        (profile?.name && [
          `for the active profile ${profile.name}, the personalized message is: ${profile.personalizedMessage}`,
        ]) || ["no profile active"]
    );
    const toCurrentYear = new Writer(
      messageModel,
      (state, newValue) => (state.message = currentYear(Date).toString())
    );

    const viewModel = {
      ...messageModel,
      messageFormatted,
      showProfile,
      toCurrentYear,
    };

    if (testMode) return viewModel;
    return [
      "div",
      { style: { color: "red" } },

      header,

      [
        "message is",
        messageFormatted,
        ["br"],

        "you can change it here:",
        textInput(message),
        ["br"],

        ui.button({
          value: "set message to current year",
          sync: toCurrentYear,
        }),
        ["br"],

        checkboxWidget({
          value: isShouting,
          label: "are you shouting at me?",
        }),
      ],
      [
        reader(
          profile,
          (_profile) =>
            (_profile?.name && [
              `for the active profile ${_profile.name}, the personalized message is: ${_profile.personalizedMessage}`,
            ]) || ["no profile active"]
        ),
      ],
    ];
  };

// Let's demonstrate using the messageDashboard

const otherDashboard = () => ["h1", "pretend I'm another big dashboard"];

const splitScreenView = () => [
  // notice that when we use "messageDashboard" in a view, we only pass in the
  // props. Ultimately, the render function will handle the view model, then
  // re-executing that other 'layer' to pass in the view model contents.
  messageDashboard({ header: ["h1", "Message Dashboard"] }),
  otherDashboard(),
];

////////////////////////////////////
/// Arrays in Model
////////////////////////////////////

const gizmosView =
  () =>
  ({ gizmos }, testMode) => {
    // gizmos.key((g) => g.id); // I think this somehow prevents total rerenders?
    gizmos.reset = new Writer((_gizmos) => {
      _gizmos.new = {
        size: "small",
        weitht: 10,
      };
    });
    gizmos.push = new Writer((_gizmos) => {
      _gizmos.new.id = Math.random();
      _gizmos.push(_gizmos.new);
    });
    if (testMode)
      return {
        gizmos,
      };
    return [
      "div",
      "here is the list of gizmos",
      gizmos.map((gizmo, index, { remove }) => [
        "div",
        ["p", "size", gizmo.size],
        ["p", "weight", gizmo.weight],
        ui.button({ label: "delete", sync: remove }),
      ]),
      [
        "div",
        "push new gizmo",
        // a "new" prop/object is automatically available for use. If push/shift/etc
        // is called without an argument, it defaults to taking whatever 'new' is.
        textInput({ content: gizmos.new.size }),
        textInput({ content: gizmos.new.weight }),
        ui.button({ label: "append new gizmo", sync: gizmos.push }),
        ui.button({ label: "preppend new gizmo", sync: gizmos.shift }),
        ui.button({ label: "reset", sync: gizmos.reset___new }),
      ],
    ];
  };

////////////////////////////////////
/// TESTS!!!!!!!
////////////////////////////////////

describe("messageDashboard view model", () => {
  it("is easy to test!!", () => {
    const messageModel = Model({
      message: "hello",
      isShouting: false,
    });

    const viewModel = messageDashboard(
      {
        models: { messageModel },
        Date: class MockDate extends Date {
          constructor() {
            super(1990, 1, 2);
          }
        },
      },
      true
    );

    console.assert(messageModel.message().get() !== "1990");
    viewModel.toCurrentYear();
    console.assert(messageModel.message().get() === "1990");
  });
});

////////////////////////////////////
/// Initializing everything with render function
////////////////////////////////////

document.body.appendChild(
  render(splitScreenView, {
    models: {
      messageModel: Model({
        message: "hello world",
        isShouting: false,
      }),
    },
  })
);

////////////////////////////////////
/// TODO
////////////////////////////////////

/**
 * * handle arrays in model.
 * * show debounce function example.
 * * fetching data from server side.
 * * "joining" models together when they have to be updated at the same time.
 */
