const basic = [
  "div",
  {
    style: {
      display: "grid",
      "grid-template-columns": "auto auto auto auto auto auto auto auto",
      "grid-template-rows": "auto auto auto auto auto auto auto auto",
      width: "500px",
      height: "500px",
    },
  },
  [
    "span",
    {
      style: {
        "grid-column": "1",
        "grid-row": "8",
        "background-color": "#deb887",
      },
    },
  ],
  [
    "span",
    {
      style: {
        "grid-column": "1",
        "grid-row": "8",
        "background-color": "#deb887",
        background:
          "url('https://upload.wikimedia.org/wikipedia/commons/7/72/Chess_rlt45.svg') no-repeat",
        "background-size": "100%",
      },
    },
  ],
];

const basic2 = [
  "div",
  {
    style: {
      display: "grid",
      "grid-template-columns": "auto auto auto auto auto auto auto auto",
      "grid-template-rows": "auto auto auto auto auto auto auto auto",
      width: "500px",
      height: "500px",
    },
  },
  [
    "span",
    {
      style: {
        "grid-column": "1",
        "grid-row": "8",
        "background-color": "#deb887",
      },
    },
  ],
  [
    "span",
    {
      style: {
        "grid-column": "1",
        "grid-row": "8",
        "background-color": "#deb887",
        background:
          "url('https://upload.wikimedia.org/wikipedia/commons/7/72/Chess_rlt45.svg') no-repeat",
        "background-size": "100%",
      },
    },
  ],
];
