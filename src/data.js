export function c2g({ rank, file }) {
  return {
    col: file,
    row: 9 - rank,
  };
}

export function startingUnits() {
  return [
    { unitType: "rook", color: "white", coordinate: { file: 1, rank: 1 } },
    {
      unitType: "knight",
      color: "white",
      coordinate: { file: 2, rank: 1 },
    },
    {
      unitType: "bishop",
      color: "white",
      coordinate: { file: 3, rank: 1 },
    },
    { unitType: "queen", color: "white", coordinate: { file: 4, rank: 1 } },
    { unitType: "king", color: "white", coordinate: { file: 5, rank: 1 } },
    {
      unitType: "bishop",
      color: "white",
      coordinate: { file: 6, rank: 1 },
    },
    {
      unitType: "knight",
      color: "white",
      coordinate: { file: 7, rank: 1 },
    },
    { unitType: "rook", color: "white", coordinate: { file: 8, rank: 1 } },
    { unitType: "pawn", color: "white", coordinate: { file: 1, rank: 2 } },
    { unitType: "pawn", color: "white", coordinate: { file: 2, rank: 2 } },
    { unitType: "pawn", color: "white", coordinate: { file: 3, rank: 2 } },
    { unitType: "pawn", color: "white", coordinate: { file: 4, rank: 2 } },
    { unitType: "pawn", color: "white", coordinate: { file: 5, rank: 2 } },
    { unitType: "pawn", color: "white", coordinate: { file: 6, rank: 2 } },
    { unitType: "pawn", color: "white", coordinate: { file: 7, rank: 2 } },
    { unitType: "pawn", color: "white", coordinate: { file: 8, rank: 2 } },

    { unitType: "rook", color: "black", coordinate: { file: 1, rank: 8 } },
    {
      unitType: "knight",
      color: "black",
      coordinate: { file: 2, rank: 8 },
    },
    {
      unitType: "bishop",
      color: "black",
      coordinate: { file: 3, rank: 8 },
    },
    { unitType: "queen", color: "black", coordinate: { file: 4, rank: 8 } },
    { unitType: "king", color: "black", coordinate: { file: 5, rank: 8 } },
    {
      unitType: "bishop",
      color: "black",
      coordinate: { file: 6, rank: 8 },
    },
    {
      unitType: "knight",
      color: "black",
      coordinate: { file: 7, rank: 8 },
    },
    { unitType: "rook", color: "black", coordinate: { file: 8, rank: 8 } },
    { unitType: "pawn", color: "black", coordinate: { file: 1, rank: 7 } },
    { unitType: "pawn", color: "black", coordinate: { file: 2, rank: 7 } },
    { unitType: "pawn", color: "black", coordinate: { file: 3, rank: 7 } },
    { unitType: "pawn", color: "black", coordinate: { file: 4, rank: 7 } },
    { unitType: "pawn", color: "black", coordinate: { file: 5, rank: 7 } },
    { unitType: "pawn", color: "black", coordinate: { file: 6, rank: 7 } },
    { unitType: "pawn", color: "black", coordinate: { file: 7, rank: 7 } },
    { unitType: "pawn", color: "black", coordinate: { file: 8, rank: 7 } },
  ];
}

export function initState() {
  return {
    board: {
      units: startingUnits(),
    },
    message: "hello world",
  };
}

const mainModel = () => ({
  board: {
    units: startingUnits(),
  },
  example: {},
});

const exampleModel = () => ({
  message: "hello world",
  setToCurrentYear() {
    this.message = "2023";
  },
});

class ExampleModel {
  message = "hello world";
  setToCurrentYear() {
    this.message = "2023";
  }
}

const exampleViewModel = viewModel(mainModel, { msg: ["message"] });

function describe() {}

describe("messages can change", () => {
  const model = use(exampleViewModel);
  assert(model(["msg"]) === "hello world");
  setToCurrentYear();
  assert(model(["msg"]) === "2023");
});

export const constants = {
  pieceImgUrls: {
    black: {
      king: "https://upload.wikimedia.org/wikipedia/commons/f/f0/Chess_kdt45.svg",
      queen:
        "https://upload.wikimedia.org/wikipedia/commons/4/47/Chess_qdt45.svg",
      rook: "https://upload.wikimedia.org/wikipedia/commons/f/ff/Chess_rdt45.svg",
      bishop:
        "https://upload.wikimedia.org/wikipedia/commons/9/98/Chess_bdt45.svg",
      knight:
        "https://upload.wikimedia.org/wikipedia/commons/e/ef/Chess_ndt45.svg",
      pawn: "https://upload.wikimedia.org/wikipedia/commons/c/c7/Chess_pdt45.svg",
    },
    white: {
      king: "https://upload.wikimedia.org/wikipedia/commons/4/42/Chess_klt45.svg",
      queen:
        "https://upload.wikimedia.org/wikipedia/commons/1/15/Chess_qlt45.svg",
      rook: "https://upload.wikimedia.org/wikipedia/commons/7/72/Chess_rlt45.svg",
      bishop:
        "https://upload.wikimedia.org/wikipedia/commons/b/b1/Chess_blt45.svg",
      knight:
        "https://upload.wikimedia.org/wikipedia/commons/7/70/Chess_nlt45.svg",
      pawn: "https://upload.wikimedia.org/wikipedia/commons/4/45/Chess_plt45.svg",
    },
  },
};
