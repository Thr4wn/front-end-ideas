import { $ } from "../yet-again.js";
import { range } from "../helpers.js";
import { c2g, constants, initState } from "./data.js";

function Model(initState = {}) {
  const eventName = "eventSystem_" + Math.random();
  document.addEventListener(
    eventName,
    (e) => e?.detail?.action && e?.detail?.action()
  );
  function runAction(action) {
    document.dispatchEvent(
      new CustomEvent(eventName, {
        detail: {
          action,
        },
      })
    );
  }

  const state = initState;
  const updaters = [];
  function update(fn) {
    runAction(() => {
      // TODO: deep copy?
      fn(state);
      // TODO: for loop and await?
      updaters.forEach((u) => u(state));
    });
  }
  function onUpdate(fn) {
    updaters.push(fn);
  }

  return {
    update,
    onUpdate,
  };
}

/////////////////////////////////////////////////////////////////
// State Helpers
/////////////////////////////////////////////////////////////////

function traverseState(obj, path, fn = (obj, key) => obj[key]) {
  const lastKey = path.pop();
  const containingObj = path.reduce((subObj, curKey) => {
    // if (isUpdate) {
    subObj[curKey] = subObj[curKey] || {};
    // }
    return subObj && subObj[curKey];
  }, obj);

  return fn(containingObj, lastKey);
}

function changeState(state, path, newValue) {
  return traverseState(state, path, (obj, key) => {
    const item = obj[key];
    if (typeof item === "function") {
      // We have to call the function _while_ dereferencing it from parent obj.
      // This is important to ensure the 'this' keyword is correct.
      return obj[key](newValue);
    } else if (Array.isArray(item)) {
      // TODO?
    } else {
      obj[key] = newValue;
      return obj[key];
    }
  });
}

function getState(state, path) {
  return traverseState(state, path);
}

function ViewModel(model, translation) {
  // somehow I have to make sure that functions are called rightly, because they
  // mutate.
  //
  // I don't think I can get away with replacement after function mutation.

  // oh wait. The real problem is that view models will not 'carry over' all the
  // model context.

  // for now, let's

  function translatedGetState(state, prop) {
    const subState = getState(state, translation[prop]);
    getState(subState, path);
  }
  function translatedChangeState(state, path, newValue) {
    const subState = setState(state, translation);
    setState(subState, path, newValue);
  }

  // This function will be used within the view function itself, to bind state
  // changes to dom changes
  const use = (path) => (fn) => {
    model.onUpdate((state) => {
      const item = getState(state, path);
      fn(item);
    });
  };

  const bind = (path) => (event) => {
    model.update((state) => {
      changeState(state, path, event.details);
    });
  };

  return {
    use,
    bind,
  };
}

/////////////////////////////////

const appState = { secondExample: { message: "hello" } };

const { use, bind } = ViewModel(model, {
  msg: ["secondExample", "message"],
});

return $([
  "div",
  { "data-message": use("msg") },
  "message is",
  use("msg"),
  [
    "input",
    {
      type: "text",
      on: {
        newValue: bind("msg"),
        // newValue: (newValue) => {
        //   bind("message", newValue);
        // },
      },
    },
  ],
  [
    "input",
    {
      type: "button",
      on: bind({
        click: viewModel("setToCurrentYear"),
      }),
      on2: {
        click: () => {
          viewModel("setToCurrentYear")();
        },
      },
    },
  ],
]);

//////

function testing() {
  const subscribe = _("message");
  ////

  const elm = document.createElement("div");

  const attrName = "data-message";
  subscribe((newValue) => {
    elm.setAttribute(attrName, newValue);
  });

  const node = document.createTextNode("");
  subscribe((newValue) => {
    node.nodeValue = newValue.toString();
  });
}

export function $main(model) {
  const vmer = (model) => ({
    ...use("message"),
  });

  return $([
    "div",
    [
      "input",
      {
        type: "text",
        on: {
          change: (event) => ["set", "message", event.target.value],
        },
      },
    ],
  ]);
  // $inputText(["set", "message"]),

  return _(
    (model) => ({
      message: model.message,
      setMessage(newMessage) {
        update((model) => (model.message = newMessage));
      },
    }),
    {
      ...use("message"),
      // message: (state) => model.message,
      // setMessage: (state, newMessage) =>
      //   update((model) => (model.message = newMessage)),
    },
    (viewModel) => {
      const { setMessage } = viewModel;
      return $(
        [
          "div",
          [
            "input",
            {
              type: "text",
              on: {
                change: (event) => ["set", "message", event.target.value],
                // change2: [
                //   (event) => event.target.value,
                //   (viewModel) => viewModel.setMessage(val),
                // ],
              },
            },
          ],
          // $inputText(["set", "message"]),
        ]
        // _((appState) => ({
        //   message: appState.secondExample.message,
        //   setMessage: (newMessage) => {
        //     appState.secondExample.message = newMessage;
        //   },
        // }))
      );
    }
  );

  // return $board(_state.board);
}

function $board(_board) {
  return $(container());

  function container() {
    return [
      "div",
      {
        style: {
          display: "grid",
          "grid-template-columns": "auto auto auto auto auto auto auto auto",
          "grid-template-rows": "auto auto auto auto auto auto auto auto",
          width: "500px",
          height: "500px",
        },
      },
      ...squares(),
      $units(_board.units),
    ];
  }
  function squares() {
    const squares = [];
    range(1, 8).forEach((file) => {
      range(1, 8).forEach((rank) => {
        const { col, row } = c2g({ rank, file });
        const evenRow = !!(row % 2);
        const evenCol = !!(col % 2);
        const hasSameEvenness = evenRow === evenCol;
        const lightShade = "#ffebcd";
        const darkShade = "#deb887";
        const shade = hasSameEvenness ? lightShade : darkShade;
        squares.push([
          "span",
          {
            style: {
              "grid-column": col,
              "grid-row": row,
              "background-color": shade,
              // 'background-image': constants[]
            },
          },
        ]);
      });
    });
    return squares;
  }
  // _`units`.map(
  //   ({ unitType, coordinate: { rank, file }, color }) => {
  // function $units() {
  //   return $(
  //     (state) => state.board.units,
  //     _`.`.map(({ unitType, coordinate: { rank, file }, color }) => {
  //       const { col, row } = c2g({ rank, file });
  //       return [
  //         "span",
  //         {
  //           style: {
  //             "grid-column": col,
  //             "grid-row": row,
  //             background: `url("${constants.pieceImgUrls[color][unitType]}") no-repeat`,
  //             "background-size": "100%",
  //           },
  //         },
  //       ];
  //     })
  //   );
  // }

  function $units(_units) {
    return _units.list(
      ({ unitType, coordinate: { rank, file }, color }) => {
        const { col, row } = c2g({ rank, file });
        return [
          "span",
          {
            style: {
              "grid-column": col,
              "grid-row": row,
              background: `url("${constants.pieceImgUrls[color][unitType]}") no-repeat`,
              "background-size": "100%",
            },
          },
        ];
      },
      ({ unitId }) => unitId
    );
  }
}
