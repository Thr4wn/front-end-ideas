// Copyright 2023 Alexander Bird
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the “Software”), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import { Model } from "./mvvm.js";

// NOTICE: this is just a set of examples using mvvm.js. To view the library
// itself, go here: ___

// testable.js

//////////////////////////////////////////////////
///// Purpose: Make Front End Code Easy to Test //
//////////////////////////////////////////////////

/**
 * Target audience: people who actively write a lot of test code for front end.
 *
 * This means that I don't care to prevent untestable code. I just need to make
 * it easy to write testable code. If someone _can_ couple things together, I am
 * not worried because test writers will just always refactor everything
 * as/because they write tests.
 *
 * Because I want to minimize lines of code, I don't want to waste lines of code
 * preventing coupling. Since target audience won't need that anyways.
 */

/**
 * I want to test state interactions.
 */

// It's easy to take an object of named 'things' (readers and writers), and then
// test them.
//
// However, I think it's annoying to put them all into one object with a weirdly
// bloated prop namespace, and then write a nested array in the view to put them
// everywhere.

// I want to be able to call another component as a function.

// I also want to have a searapte viewModel function from view function. This is
// so that I have state object to test on it's own.

// The problem with {VM, V} is that it's not a function to call. Something has
// to always call VM first, and then pass that to V. That's a lot to ask for.

// So I'm considering messComp = Component({V, VM}).
//

Component = () => {
  return V(VM(model));
};

// oof. I need to propagate the model to the component as well.
//
// Is it ever ok to propagate from inside the view?

//ok. What about this.

// but now member is also from model, and could interfere with real model input
// to VM. Shadowing is happening.
text = ({ member }) => ({
  VM(model, deps) {
    //
  },
  V() {
    //
  },
});

//
text = ({ ...model, member }, deps) => ({
  VM() {
    //
  },
  // But now the view has access to model directly. I guess that's fine.
  V() {
    //
  },
});

dashboard = (props) => ({
  VM(model, deps) {},
  V() {},
});

// I want this text widget interface:: text(member)

// I like the idea now of one input entirely for dep injection.

// deps are different than props.

/**
 * I want separate VM and V functions.
 */

text = ({ member }) => {
  return [];
};

// Ok. What if I _don't_ have viewmodel object? How hard is that to test?

// I think I'm now leaning towards render function will know what to do with
// props of VM and V.
//
// That feels clean in that object inputs to render are always how you do
// something special in a composable way.
//
// While the idea of reserving objects for only attributes also feels nice,
// there's no functional reason. Well, I guess you might want an attribute named
// View. :shrug:

// The other way is a function. The component outputs a function, and that can
// configure how the view renders.
//
// The problem with that is how do you get access to the VM?

// dashboard = (props) =>
//   Component(
//     ({ read, write }) => {
//       return {
//         item: read(true),
//       };
//     },
//     ({ item }) => {
//       return [];
//     }
//   );

text = Component(["input", { type: "text", value: member }]);

text = (member, { isTextArea = false }) => [
  isTextArea ? ["textarea"] : ["input", { type: "text" }],
  { value: member },
  { change: targetValueHandler },
];

dashboard = Component(
  ({ defaultValue }, { read, write, readWrite }, deps) => {
    return {
      item: readWrite("message"),
    };
  },
  ({ item }) => [text(item)]
);

it("", () => {
  //
  const viewModel = dashboard({ defaultValue: true }, "viewModel");
});

dashboard = (item) => {
  return [text(item)];
};

// ok. So it's easy to write views. Name parameters whatever is intuitive and you need!

// So now comes the hard part of how to connect that to model.

/*
 * 1. I want to write a native js function/component that does everything I
 *    want.
 * 2. Then pull out the bare minimum code that's using browser interface/API.
 * 3. Now my function is really easy to test (dependency injection pattern)
 * 4. I only needed two helper functions to make that easy to do (render and
 *    model)
 * 5. I also saved a collection of widgets for how I like to do things.
 */

/////////////// Principles

// I seem to be discovering a design pattern. Always use more 'toggles', and
// then put all conditional logic inside readers/viewModel strings.

// I also don't want to write pathing boiler plate.
//
// And I don't want non-traversal logic in completely separate functions for
// more testability.

////////////////////////////////////////////////
///// Demo Helpers /////////////////////////////
////////////////////////////////////////////////

// These helpers are here to make the examples easier to read

// Dummy functions to emulate a test suite
const describe = (text, fn) => {
  console.group(text);
  fn();
  console.groupEnd();
};
const it = describe;

/**
 * @description Easily get/set nested properties of an object.
 *
 * @example
 * traverse({a: {b: 10}}, ['a', 'b']) === 10
 *
 * incrementAB(obj) {
 *   traverse(obj, ['a', 'b'], (parent, lastKey) => parent[lastKey]++)
 * }
 * obj = {a: {b: 10}}
 * incrementAB(obj)
 * traverse(obj, ['a', 'b']) === 11
 */
function traverse(obj, path, fn = (obj, key) => obj[key]) {
  const lastKey = path.pop();
  const containingObj = path.reduce((subObj, curKey) => {
    // if (isUpdate) {
    subObj[curKey] = subObj[curKey] || {};
    // }
    return subObj && subObj[curKey];
  }, obj);

  return fn(containingObj, lastKey);
}

// A minimal "debounce" function for demo purposes.
function debounce(func, timeout = 300) {
  let timer;
  return (...args) => {
    clearTimeout(timer);
    timer = setTimeout(() => {
      func.apply(this, args);
    }, timeout);
  };
}

//////////////////////////////////
/// Shared "Widget" Components ///
//////////////////////////////////

// To use (and get value from) this library, you have to know about the custom
// 'sync' event it uses. It's central to how everything works, and a solid
// design pattern.

/*
 * What happens on DOM Events:
 *
 * 1. A dom event fires
 * 2. An event handler parses out the useful value (like event.target.value for
 *    example)
 * 3. A custom "sync" event must be fired, being passed **only** the logical
 *    value of interest (the output of step 2 above, not the DOM event object
 *    itself)
 * 4. A 'view-model handler' is attached to the "sync" event. When triggered, it
 *    will only recieve the logical value itself.
 *
 * Benefits:
 *
 * 1. This ensures that view-model handlers never get coupled with annoying dom
 *    API stuctures. It only contains front-end business logic.
 * 2. This decouples other annoying logistics (like debouncing) from the
 *    business logic as well.
 */

// These are the things I do _not_ want to automate component/unit test. If we
// decide to add/remove debouncing, I don't really care about that in unit
// testing. So I throw all the stuff I don't want to unit test into a function.
// But _only_ the minimum I don't want to test. Once the 'sync' event fires,
// everything after that should be stuff I want to test.
//
// If this changes to a textarea, fine. That has nothing to do with the state
// logic I'm trying to unit test.
//
// By putting it into a function:
// 1. It makes deep equality checks easier (vs inlining array templates)
// 2. I want to see if DI can be used.
//
// I'm using the word 'primatives' to mean these things that ONLY deal with
// browser specifics. I start them with underscore to visually distinguish these
// things from other lower level widget components. Because even with lower
// level widget components, I want to include that in my testing. It's only the
// browser interface itself I want to separate out.
const primatives = {
  _textInput: () => [
    "input",
    {
      type: "text",
      on: {
        keyup(event) {
          debounce(() => {
            // The "sync" function is built-in to the library. It calls
            // `element.dispatchEvent("sync", ...)` and passes a single value to
            // it. In this case, it will be event.target.value.
            sync(event.target, event.target.value);
          });
        },
      },
    },
  ],
  _button: () => [
    "input",
    {
      type: "button",
      on: {
        click(event) {
          sync(event.target, true);
        },
      },
    },
  ],
};

const widgets = {
  text(modelItem) {
    return [primatives._textInput, { value: modelItem }, { sync: modelItem }];
    //
  },
};

////////////////////////
////// Example 1  //////
////////////////////////

const initState = {
  title: "Showing off this library",
  contents: {
    notification: "hello",
    favoriteNumber: 0,
    footer: "This page is an example",
  },
};

/**
 * The complete Triforce, or one or more components of the Triforce.
 * @typedef {Object} Model
 * @property {function} read - Read state
 * @property {function} write - Write state
 */

//
const defaultDependencies = {
  Date,
  fetch,
  widgets,
};

/**
 * @param {Model} model The read/write/readWrite functions for updating state.
 * They're already bound to certain state.
 * @param {object} dependecies Anything you want to dependency inject for
 * testing.
 */
const messageDashboard = (
  { read, write, readWrite },
  { Date, widgets: { textInput, button } } = defaultDependencies
) => {
  const paths = {
    message: ["contents", "notification"],
    footer: ["contents", "footer"],
  };

  const magicNumber = 42; // component-specific constant.
  const message = readWrite(paths.message);
  const toCurrentYear = write((state, newValue) => {
    traverse(
      state,
      paths.message,
      (obj, key) => (obj[key] = new Date().getFullYear())
    );
  });
  const footer = read(fromPath(paths.footer));

  return [
    "div",
    styles.demoRed,
    // { style: { color: "red" } },

    "message is",
    // By passing in 'message' (which is technically a function), it will be
    // turned into a text node whose value updates automatically.
    message,
    ["br"],

    "you can change it here:",
    // syncTo returns "{on: {sync: ...}}"
    [textInput, { value: message }, syncTo(message)],
    ["br"],

    "or .... you can change it here:",
    // bind.input is a shortand for: {value: message}, syncTo(message)
    [inputText, bind.input(message)],
    ["br"],

    ".... OR .... you can change it here:",
    // 'text' built-in is same as above; plus options for doing textarea and
    // more.
    widgets.text(message),

    [inputButton, { sync: toCurrentYear }],
  ];
};

const $messageDashboard = (props) => ({
  // The _output_ is technically the view model. This generates it. Hence the
  // name starts with a capital letter.
  ViewModel: ({ read, write, readWrite }, { Date } = defaultDependencies) => {
    const paths = {
      message: ["contents", "notification"],
      footer: ["contents", "footer"],
    };
    return {
      magicNumber: 42, // a constant for only this comonent.
      message: readWrite(paths.message),
      toCurrentYear: write((state, newValue) => {
        traverse(
          state,
          paths.message,
          (obj, key) => (obj[key] = new Date().getFullYear())
        );
      }),
      footer: read(fromPath(paths.footer)),
    };
  },
  View: ({ message, toCurrentYear, footer }) => [
    "div",
    { style: { color: "red" } },

    "message is",
    // By passing in 'message' (which is technically a function), it will be
    // turned into a text node whose value updates automatically.
    message,
    ["br"],

    "you can change it here:",
    // syncTo returns "{on: {sync: ...}}"
    [inputText, { value: message }, syncTo(message)],
    ["br"],

    "or ... you can change it here:",
    // bind.input is a shortand for: {value: message}, syncTo(message)
    [inputText, bind.input(message)],
    ["br"],

    ".... OR .... you can change it here:",
    // Using the previous example component
    text(message),

    [inputButton, { sync: toCurrentYear }],
  ],
});

describe("messageDashboard", () => {
  it("is easy to test!!", () => {
    // 'testMode' means it won't actually create dom elements. The read/write
    // functions won't do any real wrapping of logic like normal. It also
    // exposes a state() function for direct access.
    const model = Model(initState, { testMode: true });

    const viewModel = $messageDashboard().ViewModel(model, {
      Date: class MockDate extends Date {
        constructor() {
          super(1990, 1, 2);
        }
      },
    });

    console.assert(model.state().contents.notification !== "1990");
    viewModel.toCurrentYear();
    console.assert(model.state().contents.notification === "1990");
  });
});

const counter = [
  (_) => [
    "div",
    "number: ",
    [
      inputText,
      "input",
      { type: "text", value: _("number"), on: { change: _("update") } },
    ],
    ["br"],
    ["input", { type: "button", on: { change: _("increment") } }, "this year"],
    ["input", { type: "button", on: { change: _("decrement") } }, "this year"],
  ],
  // VIEW MODEL part of the component.
  (model, { Date }) => ({
    message: {
      use() {
        return model.use("contents", "notification");
      },
      update(newMessage) {
        return model.update(
          (state) => (state.contents.notification = newMessage)
        );
      },
    },
    // The 'translate' helper does the same as above.
    favoriteNumber: model.translate("contents", "favoriteNumber"),
  }),
];
