// Copyright 2023 Alexander Bird
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the “Software”), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import { isObject } from "./helpers.js";

function read(reader) {
  const node = document.createTextNode();
  model.onUpdate((state) => {
    // const value = getState(state, ["contents", "notification"]);
    const value = reader(state);
    node.textContent = value && value.toString();
  });
}

// function makeViewModelOutput(model) {
//   return {
//     $message() {
//       const node = document.createTextNode("");
//       model.onUpdate((state) => {
//         const value = getState(state, ["contents", "notification"]);
//         node.textContent = value && value.toString();
//       });
//     },
//     _message: {
//       on: {
//         sync(event) {
//           model.update((state) => {
//             changeState(state, ["contents", "notification"], event.details);
//           });
//         },
//       },
//     },
//   };
// }

function Model(initState = {}) {
  const eventName = "eventSystem_" + Math.random();
  document.addEventListener(
    eventName,
    (e) => e?.detail?.action && e?.detail?.action()
  );
  function runAction(action) {
    document.dispatchEvent(
      new CustomEvent(eventName, {
        detail: {
          action,
        },
      })
    );
  }

  const state = initState;
  const updaters = [];
  function update(fn) {
    runAction(() => {
      // TODO: deep copy?
      fn(state);
      // TODO: for loop and await?
      updaters.forEach((u) => u(state));
    });
  }
  function onUpdate(fn) {
    updaters.push(fn);
  }

  function __testState() {
    return state;
  }

  return {
    update,
    onUpdate,
    __testState,
  };
}

function traverseState(obj, path, fn = (obj, key) => obj[key]) {
  const lastKey = path.pop();
  const containingObj = path.reduce((subObj, curKey) => {
    // if (isUpdate) {
    subObj[curKey] = subObj[curKey] || {};
    // }
    return subObj && subObj[curKey];
  }, obj);

  return fn(containingObj, lastKey);
}

function changeState(state, path, newValue) {
  return traverseState(state, path, (obj, key) => {
    const item = obj[key];
    if (typeof item === "function") {
      // We have to call the function _while_ dereferencing it from parent obj.
      // This is important to ensure the 'this' keyword is correct.
      return obj[key](newValue);
    } else if (Array.isArray(item)) {
      // TODO?
    } else {
      obj[key] = newValue;
      return obj[key];
    }
  });
}

function getState(state, path) {
  return traverseState(state, path);
}

function ViewModel(model, translation) {
  // somehow I have to make sure that functions are called rightly, because they
  // mutate.
  //
  // I don't think I can get away with replacement after function mutation.

  // oh wait. The real problem is that view models will not 'carry over' all the
  // model context.

  // for now, let's

  function translatedGetState(state, prop) {
    const subState = getState(state, translation[prop]);
    getState(subState, path);
  }
  function translatedChangeState(state, path, newValue) {
    const subState = setState(state, translation);
    setState(subState, path, newValue);
  }

  // This function will be used within the view function itself, to bind state
  // changes to dom changes
  const use = (path) => (fn) => {
    model.onUpdate((state) => {
      const item = getState(state, path);
      fn(item);
    });
  };

  const bind = (path) => (event) => {
    model.update((state) => {
      changeState(state, path, event.detail);
    });
  };

  return {
    use,
    bind,
  };
}

export function View(structure) {
  const { type, attrs, content } = getStructureParts(structure);

  const node = type
    ? document.createElement(type)
    : document.createDocumentFragment();

  attrs.on &&
    Object.entries(attrs.on).map(([eventName, handler]) => {
      node.addEventListener(eventName, handler);
    });
  delete attrs.on;

  attrs.style &&
    Object.entries(attrs.style).map(([styleName, value]) => {
      node.style[styleName] = value;
    });
  delete attrs.style;

  Object.entries(attrs).map(([attr, value]) => {
    if (typeof value === "function") {
      value((newValue) => node.setAttribute(attr, newValue));
    } else {
      node.setAttribute(attr, value);
    }
  });

  const convertedItems = content.map((item) => {
    if (Array.isArray(item)) {
      return $(item);
    } else if (typeof item == "function") {
      const node = document.createTextNode();
      return item((newValue) => (node.textContent = newValue.toString()));
    } else {
      return document.createTextNode(item.toString());
    }
  });
  convertedItems.forEach((item) => {
    node.appendChild(item);
  });

  return node;

  // "structure" data type described elsewhere ['div', {on: {click() {}}},
  // ['span', 'hi']]. Because type and attrs are optional, we must do work to
  // figure out the real type (explicit, implit, or none), the attrs (provided or
  // empty), and the content (includes or not includes the first and second items)
  function getStructureParts(structure) {
    const first = structure[0];
    const typeExplicit = typeof first === "string";

    const attrsIndex = typeExplicit ? 1 : 0;
    const possibleAttrs = structure[attrsIndex];
    const attrsProvided = isObject(possibleAttrs);

    const typeImplicit = !typeExplicit && attrsProvided;

    const indexContentStarts =
      0 + (typeExplicit ? 1 : 0) + (attrsProvided ? 1 : 0);

    return {
      type: typeExplicit ? first : typeImplicit ? "span" : null,
      attrs: attrsProvided ? possibleAttrs : {},
      content: structure.slice(indexContentStarts),
    };
  }
}

//////////////////////////////////////////////////////
//////////////////////////////////////////////////////

function onSync(element, handler) {
  return element.addEventListener("sync", (event) =>
    handler(event && event.detail)
  );
}

function sync(element, detail) {
  return element.dispatchEvent("sync", new CustomEvent("sync", { detail }));
}

function debounce(func, timeout = 300) {
  let timer;
  return (...args) => {
    clearTimeout(timer);
    timer = setTimeout(() => {
      func.apply(this, args);
    }, timeout);
  };
}
